//
//  MapEnvironment+Mock
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Map

public extension MapEnvironment {

    static let mock = Self(client: .live)
}
