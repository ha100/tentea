//
//  LoginEnvironment+Mock
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Login

public extension LoginEnvironment {

    static let lockedFaceIDMock = Self(client: .lockedFaceIDMock,
                                       mainQueue: .immediate)

    static let unlockedFaceIDMock = Self(client: .unlockedFaceIDMock,
                                         mainQueue: .immediate)

    static let lockedTouchIDMock = Self(client: .lockedTouchIDMock,
                                        mainQueue: .immediate)

    static let unlockedTouchIDMock = Self(client: .unlockedTouchIDMock,
                                          mainQueue: .immediate)

    static let lockedSimulatorMock = Self(client: .lockedSimulatorMock,
                                          mainQueue: .immediate)

    static let unlockedSimulatorMock = Self(client: .unlockedSimulatorMock,
                                            mainQueue: .immediate)
}
