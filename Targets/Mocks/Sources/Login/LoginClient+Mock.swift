//
//  LoginClient+Mock
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Login

public extension LoginClient {

    static let lockedFaceIDMock = Self(authenticate: { _ in

        Effect(value: LoginResponse())

    }, persistProtectionState: { state in

        Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        .locked
    }, supportedProtection: {

        .face
    })

    static let unlockedFaceIDMock = Self(authenticate: { _ in

        Effect(value: LoginResponse())

    }, persistProtectionState: { state in

        Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        .unlocked
    }, supportedProtection: {

        .face
    })

    static let lockedTouchIDMock = Self(authenticate: { _ in

        Effect(value: LoginResponse())

    }, persistProtectionState: { state in

        Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        .locked
    }, supportedProtection: {

        .touch
    })

    static let unlockedTouchIDMock = Self(authenticate: { _ in

        Effect(value: LoginResponse())

    }, persistProtectionState: { state in

        Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        .unlocked
    }, supportedProtection: {

        .touch
    })

    static let lockedSimulatorMock = Self(authenticate: { _ in

        Effect(value: LoginResponse())

    }, persistProtectionState: { state in

        Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        .locked
    }, supportedProtection: {

        .none
    })

    static let unlockedSimulatorMock = Self(authenticate: { _ in

        Effect(value: LoginResponse())

    }, persistProtectionState: { state in

        Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        .unlocked
    }, supportedProtection: {

        .none
    })
}
