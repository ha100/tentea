//
//  Data+FromFile
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

public extension Data {

    // MARK: - Types

    enum DataError: Error {
        case url(file: String)
    }

    enum Keys {
        static let fileExtension = "csv"
    }

    // MARK: - LifeCycle

    /// parses `Data` from `Mocks` bundle based on given filename
    ///
    /// - Parameter name: string name of the resource file without extension
    /// - Returns: raw `Data`
    ///
    static func fromCsvFile(name: String) throws -> Data {

        guard let url = Bundle.module
                .resourceURL
                .flatMap({ try? FileManager.default.contentsOfDirectory(at: $0, includingPropertiesForKeys: nil) })
                .flatMap({ $0.first(where: { $0.absoluteString.contains(name + "." + Keys.fileExtension) }) }) else {

                throw DataError.url(file: name)
            }

        return try Data(contentsOf: url)
    }
}
