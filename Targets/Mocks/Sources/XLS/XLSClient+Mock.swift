//
//  XLSClient+Mock
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Model
import XLS

public extension XLSClient {

    /// mock implementation
    ///
    static let emptyMock = Self { _ in

        Effect(value: .receivedData(Data()))

    } parseFrom: { string in

        Effect(value: .parsedEntries(Entry.parseFrom(string: string)))
    }

    /// mock implementation
    ///
    static let mock = Self { stringURL in

        guard let data = try? Data.fromCsvFile(name: stringURL) else {

            return .none
        }

        return Effect(value: .receivedData(data))

    } parseFrom: { string in

        Effect(value: .parsedEntries(Entry.parseFrom(string: string)))
    }
}
