//
//  XLSEnvironment+Mock
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import XLS

public extension XLSEnvironment {

    static let mock = Self(client: .mock)
}
