//
//  LoginClient
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation
import LocalAuthentication

/// type responsible for biometry authentication
///
public struct LoginClient {

    // MARK: - Properties

    let authenticate: (String) -> Effect<LoginResponse, LoginError>
    let persistProtectionState: (LoginStatus) -> Effect<LoginAction, Never>
    let currentProtectionState: () -> LoginStatus
    let supportedProtection: () -> LoginBiometry

    // MARK: - Init

    public init(authenticate: @escaping (String) -> Effect<LoginResponse, LoginError>,
                persistProtectionState: @escaping (LoginStatus) -> Effect<LoginAction, Never>,
                currentProtectionState: @escaping () -> LoginStatus,
                supportedProtection: @escaping () -> LoginBiometry) {

        self.authenticate = authenticate
        self.persistProtectionState = persistProtectionState
        self.currentProtectionState = currentProtectionState
        self.supportedProtection = supportedProtection
    }
}

public extension LoginClient {

    enum Keys {
        static let persistance = "protection"
    }

    /// default implementation reaching out to live apple `LocalAuthentication` framework
    ///
    static let live = Self(authenticate: { reason in

        Effect.task {

            /// determine if app has the permissions required to use biometric authentication
            ///
            // TODO: code duplication - perhaps explore ways to extract
            var canAuthenticateWithBiometrics: Bool {

                get throws {

                    var error: NSError?
                    let permissions = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)

                    if let error = error {
                        throw error
                    }

                    return permissions
                }
            }

            var supportsBiometry: Bool {

                switch context.biometryType {

                    case .faceID, .touchID: return true

                    case .none: return false

                    @unknown default: return false
                }
            }

            /// object that will be used to interact with both Face ID and Touch ID
            ///
            let context = LAContext()
            context.localizedFallbackTitle = ""

            guard try canAuthenticateWithBiometrics && supportsBiometry else {
                throw LoginError.notAvailable
            }

            guard let success = try? await context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                                                  localizedReason: reason),
                  success == true else {

                throw LoginError.failed
            }

            return .init()
        }
        .mapError { error -> LoginError in

            if let error = error as? LAError {
                return .laerror(error.localizedDescription)
            }

            return .failed
        }
        .eraseToEffect()
    }, persistProtectionState: { state in

        /// storing protection status in dafaults is lets just say
        /// not best and cryptographically sound practice,
        /// but suffisient for the sake of demo
        ///
        UserDefaults.standard.set(state.rawValue, forKey: Keys.persistance)

        return Effect(value: .protectionLevelChanged(state))

    }, currentProtectionState: {

        guard let string = UserDefaults.standard.string(forKey: Keys.persistance),
              let state = LoginStatus(rawValue: string) else {

            return .unknown
        }

        return state
    }, supportedProtection: {

        /// determine if app has the permissions required to use biometric authentication
        ///
        // TODO: code duplication - perhaps explore ways to extract
        var canAuthenticateWithBiometrics: Bool {

            var error: NSError?
            let permissions = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)

            if let error = error {
                return false
            }

            return permissions
        }

        let context = LAContext()

        guard canAuthenticateWithBiometrics else {
            return .none
        }

        switch context.biometryType {

            case .faceID: return .face
            case .touchID: return .touch

            case .none: return .none

            @unknown default: return .none
        }
    }
    )
}
