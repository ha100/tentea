//
//  LoginEnvironment
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation

public struct LoginEnvironment {

    public let client: LoginClient
    let mainQueue: AnySchedulerOf<DispatchQueue>

    public init(client: LoginClient,
                mainQueue: AnySchedulerOf<DispatchQueue>) {

        self.client = client
        self.mainQueue = mainQueue
    }
}

public extension LoginEnvironment {

    static let live = Self(client: .live,
                           mainQueue: .main)
}
