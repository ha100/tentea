//
//  LoginState
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation

public struct LoginState: AutoEquatable {

    // MARK: - Properties

    public var alert: AlertState<LoginAction>?
    public var status: LoginStatus = .unknown
    public var supportedBiometry: LoginBiometry = .none

    // MARK: - Init

    public init(alert: AlertState<LoginAction>? = nil) {
        self.alert = alert
    }
}
