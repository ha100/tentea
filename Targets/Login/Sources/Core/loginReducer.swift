//
//  LoginReducer
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Combine
import ComposableArchitecture
import Foundation

public let loginReducer = Reducer<LoginState, LoginAction, LoginEnvironment> { state, action, environment in

    switch action {

        case let .protectionLevelChanged(status):

            state.status = status
            return .none

        case .authenticate:

            return environment
                .client
                .authenticate("Log in with Biome3x")
                .receive(on: environment.mainQueue)
                .catchToEffect(LoginAction.loginResponse)

        case .protect:

            let status = environment
                .client
                .currentProtectionState()

            switch status {

                case .locked:

                    return environment
                        .client
                        .persistProtectionState(.unlocked)

                default:

                    return environment
                        .client
                        .persistProtectionState(.locked)
            }

        case .onAppear:

            let status = environment
                .client
                .currentProtectionState()

            state.supportedBiometry = environment
                .client
                .supportedProtection()

            return Effect(value: .protectionLevelChanged(status))

          case .onDisappear:

            return .none

        case let .loginResponse(.failure(error)):

            state.alert = .init(title: TextState("Error"),
                                message: TextState(error.localizedDescription),
                                dismissButton: .default(TextState("Ok"),
                                                        action: .send(.dismissAlert)))
            return .none

        case .loginResponse:

            return environment
                .client
                .persistProtectionState(.unlocked)

        case .dismissAlert:

            state.alert = nil
            return .none
    }
}
.debug()
