//
//  LoginAction
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

public enum LoginAction: AutoEquatable {

    case dismissAlert
    case loginResponse(Result<LoginResponse, LoginError>)
    case protect
    case authenticate
    case protectionLevelChanged(LoginStatus)

    case onAppear
    case onDisappear
}
