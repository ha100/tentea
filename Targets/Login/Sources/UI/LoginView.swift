//
//  Loginiew
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Helpers
import SwiftUI

/// main view for protected section of the app that works with biometry via `LocalAuthentication` framework
///
public struct LoginView: View {

    // MARK: - Types

    enum Keys {
        static let face = "faceid"
        static let touch = "touchid"
    }

    // MARK: - Properties

    private let store: Store<LoginState, LoginAction>

    /// main view body displaying simple pretection status `Label` and `Button` to change the protection
    ///
    public var body: some View {

        WithViewStore(store) { viewStore in

            ZStack {

                Color.back
                    .edgesIgnoringSafeArea(.all)

                VStack {

                    Text(viewStore.status.rawValue)
                        .foregroundColor(Color.border)
                        .padding()

                    Button {
                        viewStore.status == .locked ? viewStore.send(.authenticate) : viewStore.send(.protect)
                    } label: {

                        Label(buttonLabel(for: viewStore.status),
                              systemImage: imageType(for: viewStore.supportedBiometry))
                        .foregroundColor(Color.white)
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .background(
                            RoundedRectangle(cornerRadius: 8)
                                .foregroundColor(Color.tentea)
                        )
                    }
                    .padding(50)
                    .alert(self.store.scope(state: \.alert),
                           dismiss: .dismissAlert)
                    .onAppear { viewStore.send(.onAppear) }
                    .onDisappear { viewStore.send(.onDisappear) }
                }
            }
        }
    }

    // MARK: - Init

    /// `LoginView` init allowing `Store<LoginState, LoginAction>` injection for uniTesting
    ///
    /// - Parameter store: `.live` or `.mock` implementation of the store
    ///
    public init(store: Store<LoginState, LoginAction>) {

        self.store = store
    }

    // MARK: - LifeCycle

    /// generates `String` to be used for `Button` label for a specified `LoginStatus`
    ///
    /// - Parameter status: current status of the protected section of app
    /// - Returns: `"Unlock"` for protected or `"Protect"` for unlocked app section
    ///
    private func buttonLabel(for status: LoginStatus) -> String {
        status == .locked ? "login.view.button.unlock".localized : "login.view.button.protect".localized
    }

    /// generates `String` to be used for `Image` display inside the `Button`
    /// for supported `LoginBiometry` of the device
    ///
    /// - Parameter biometry: currently supported biometry by device
    /// - Returns: • `""` for no biometry - simulator
    ///            • `"faceid"` for faceID
    ///            • `"touchid"` for capable devices
    ///
    private func imageType(for biometry: LoginBiometry) -> String {

        guard biometry != .none else {
            return "stop"
        }

        return biometry == .face ? Keys.face : Keys.touch
    }
}
