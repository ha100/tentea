//
//  LoginStatus
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

/// type specifying current state of the protected section of the app
///
public enum LoginStatus: String, Equatable {

    case unknown
    case unlocked
    case locked
}
