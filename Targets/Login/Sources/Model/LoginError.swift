//
//  LoginError
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

/// descriptive error that provides more information about any errors that occur during authentication
///
public enum LoginError: Error, AutoEquatable {

    case notAvailable
    case failed
    case laerror(String)
}
