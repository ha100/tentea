//
//  LoginBiometry.swift
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

/// type representing supported biometry type
///
public enum LoginBiometry: Equatable {

    case face
    case touch
    case none
}
