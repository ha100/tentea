//
//  LoginTests
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation
import Model
import SnapshotTesting
import SwiftUI
import XCTest
@testable import Mocks
@testable import Login

final class LoginTests: XCTestCase {

    func test_Unlocked_TouchID_LoginView_Looks_AsExpected() {

        let store = Store(initialState: LoginState(),
                          reducer: loginReducer,
                          environment: .unlockedTouchIDMock)

        let controller = UIHostingController(rootView: LoginView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }

    func test_Locked_TouchID_LoginView_Looks_AsExpected() {

        let store = Store(initialState: LoginState(),
                          reducer: loginReducer,
                          environment: .lockedTouchIDMock)

        let controller = UIHostingController(rootView: LoginView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }

    func test_Unlocked_FaceID_LoginView_Looks_AsExpected() {

        let store = Store(initialState: LoginState(),
                          reducer: loginReducer,
                          environment: .unlockedFaceIDMock)

        let controller = UIHostingController(rootView: LoginView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }

    func test_Locked_FaceID_LoginView_Looks_AsExpected() {

        let store = Store(initialState: LoginState(),
                          reducer: loginReducer,
                          environment: .lockedFaceIDMock)

        let controller = UIHostingController(rootView: LoginView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }

    func test_Unlocked_Simulator_LoginView_Looks_AsExpected() {

        let store = Store(initialState: LoginState(),
                          reducer: loginReducer,
                          environment: .unlockedSimulatorMock)

        let controller = UIHostingController(rootView: LoginView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }

    func test_Locked_Simulator_LoginView_Looks_AsExpected() {

        let store = Store(initialState: LoginState(),
                          reducer: loginReducer,
                          environment: .lockedSimulatorMock)

        let controller = UIHostingController(rootView: LoginView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }
}
