//
//  MapView
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import MapKit
import SwiftUI

/// main view for cartography section of the app that works with location
/// via `CoreLocation` and `Mapkit` frameworks
///
public struct MapView: View {

    // MARK: - Properties

    private let store: Store<MapState, MapAction>

    /// main view body displaying map and users current location
    ///
    public var body: some View {

        WithViewStore(self.store) { viewStore in

            ZStack {

                LocationView(region: viewStore.binding(get: { $0.region },
                                                       send: MapAction.updateRegion))
                .edgesIgnoringSafeArea([.all])

            }
            .alert(self.store.scope { $0.alert },
                   dismiss: .dismissAlertButtonTapped)
            .onAppear { viewStore.send(.onAppear) }
            .onDisappear { viewStore.send(.onDisappear) }
        }
    }

    // MARK: - Init

    /// `LoginView` init allowing `Store<LoginState, LoginAction>` injection for uniTesting
    ///
    /// - Parameter store: `.live` or `.mock` implementation of the store
    ///
    public init(store: Store<MapState, MapAction>) {

        self.store = store
    }
}
