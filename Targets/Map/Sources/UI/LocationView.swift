//
//  LocationView
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import MapKit
import SwiftUI

public struct LocationView: UIViewRepresentable {

    // MARK: - Properties

    @Binding var region: CoordinateRegion?

    // MARK: - Init

    public init(region: Binding<CoordinateRegion?>) {

        self._region = region
    }

    // MARK: - LifeCycle

    public func makeUIView(context: Context) -> MKMapView {
        self.makeView(context: context)
    }

    public func updateUIView(_ mapView: MKMapView, context: Context) {
        self.updateView(mapView: mapView, delegate: context.coordinator)
    }

    public func makeCoordinator() -> MapViewCoordinator {
        MapViewCoordinator(self)
    }

    private func makeView(context: Context) -> MKMapView {

        let mapView = MKMapView(frame: .zero)
        mapView.showsUserLocation = true

        return mapView
    }

    private func updateView(mapView: MKMapView, delegate: MKMapViewDelegate) {

        mapView.delegate = delegate

        if let region = self.region {
            mapView.setRegion(region.asMKCoordinateRegion, animated: true)
        }
    }
}
