//
//  CoordinateRegion
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import CoreLocation
import Foundation
import MapKit

/// type representing displayed region on the map
///
public struct CoordinateRegion: AutoEquatable {

    // MARK: - Properties

    /// location latitude and longitude
    ///
    public var center: CLLocationCoordinate2D

    /// zoom level of the map
    ///
    public var span: MKCoordinateSpan

    public var asMKCoordinateRegion: MKCoordinateRegion {
        .init(center: self.center, span: self.span)
    }

    // MARK: - Init

    public init(center: CLLocationCoordinate2D,
                span: MKCoordinateSpan) {

        self.center = center
        self.span = span
    }

    public init(coordinateRegion: MKCoordinateRegion) {

        self.center = coordinateRegion.center
        self.span = coordinateRegion.span
    }
}
