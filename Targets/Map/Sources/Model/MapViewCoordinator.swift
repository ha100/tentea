//
//  MapViewCoordinator
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import MapKit

public class MapViewCoordinator: NSObject, MKMapViewDelegate {

    // MARK: - Properties

    var mapView: LocationView

    // MARK: - Init

    init(_ control: LocationView) {
        self.mapView = control
    }

    // MARK: - LifeCycle

    public func mapView(_ mapView: MKMapView,
                        regionDidChangeAnimated animated: Bool) {

        self.mapView.region = CoordinateRegion(coordinateRegion: mapView.region)
    }
}
