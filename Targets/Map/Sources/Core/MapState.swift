//
//  MapState
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableCoreLocation
import Foundation
import MapKit

public struct MapState: AutoEquatable {

    // MARK: - Properties

    public var alert: AlertState<MapAction>?
    public var isRequestingCurrentLocation = false
    public var region: CoordinateRegion?

    // MARK: - Init

    public init(alert: AlertState<MapAction>? = nil,
                isRequestingCurrentLocation: Bool = false,
                region: CoordinateRegion? = nil) {

        self.alert = alert
        self.isRequestingCurrentLocation = isRequestingCurrentLocation
        self.region = region
    }
}
