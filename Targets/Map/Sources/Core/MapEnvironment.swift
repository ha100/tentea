//
//  MapEnvironment
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import ComposableCoreLocation
import Foundation

public struct MapEnvironment {

    // MARK: - Properties

    public let client: LocationManager

    // MARK: - Init

    public init(client: LocationManager) {

        self.client = client
    }
}

public extension MapEnvironment {

    static let live = Self(client: .live)
}
