//
//  MapReducer
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation
import MapKit

public let mapReducer = Reducer<MapState, MapAction, MapEnvironment> { state, action, environment in

    struct LocationManagerId: Hashable {}

    switch action {

        case .currentLocationButtonTapped:

            guard environment.client.locationServicesEnabled() else {
                state.alert = .init(title: TextState("Location services are turned off."))
                return .none
            }

            switch environment.client.authorizationStatus() {

                case .notDetermined:

                    state.isRequestingCurrentLocation = true

                    return environment.client
                        .requestWhenInUseAuthorization()
                        .fireAndForget()

                case .restricted:

                    state.alert = .init(title: TextState("Please give us access to your location in settings."))
                    return .none

                case .denied:

                    state.alert = .init(title: TextState("Please give us access to your location in settings."))
                    return .none

                case .authorizedAlways, .authorizedWhenInUse:

                    return environment.client
                        .requestLocation()
                        .fireAndForget()

                @unknown default: return .none
            }

        case .dismissAlertButtonTapped:

            state.alert = nil
            return .none

        case .onAppear:

            return environment.client.delegate()
              .map(MapAction.locationManager)
              .cancellable(id: LocationManagerId())

          case .onDisappear:
            return .cancel(id: LocationManagerId())

        case let .updateRegion(region):

            state.region = region
            return .none

        case .locationManager: return .none
    }
}
.combined(with: locationManagerReducer
    .pullback(state: \.self, action: /MapAction.locationManager) { $0 }
)
.debug()
