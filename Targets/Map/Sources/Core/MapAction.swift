//
//  PhantomProtocolMapAction
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableCoreLocation
import Foundation
import MapKit

public enum MapAction: AutoEquatable {

    case currentLocationButtonTapped
    case dismissAlertButtonTapped
    case locationManager(LocationManager.Action)
    case updateRegion(CoordinateRegion?)

    case onAppear
    case onDisappear
}
