//
//  LocationManagerReducer
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import ComposableCoreLocation
import Foundation
import MapKit

internal let locationManagerReducer = Reducer<MapState, LocationManager.Action, MapEnvironment> { state, action, environment in

    switch action {

        case .didChangeAuthorization(.authorizedAlways),
             .didChangeAuthorization(.authorizedWhenInUse):

            if state.isRequestingCurrentLocation {

                return environment.client
                    .requestLocation()
                    .fireAndForget()
            }

            return .none

        case .didChangeAuthorization(.denied),
            .didChangeAuthorization(.restricted):

            if state.isRequestingCurrentLocation {

                state.alert = .init(title: TextState("Please give location access so that we can show you some cool stuff."))
                state.isRequestingCurrentLocation = false
            }

            return .none

        case let .didUpdateLocations(locations):

            state.isRequestingCurrentLocation = false

            guard let location = locations.first else { return .none }

            state.region = CoordinateRegion(center: location.coordinate,
                                            span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            return .none

        default: return .none
    }
}
