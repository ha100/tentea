//
//  CLLocationCoordinate2D+
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import CoreLocation

extension CLLocationCoordinate2D: Hashable {

    public func hash(into hasher: inout Hasher) {

        hasher.combine(latitude)
        hasher.combine(longitude)
    }
}

extension CLLocationCoordinate2D: AutoEquatable {}
