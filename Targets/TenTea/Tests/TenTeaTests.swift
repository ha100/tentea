//
//  TenTeaTests
//  TenTea
//
//  Created by tom Hastik on 21/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation
import SnapshotTesting
import SwiftUI
import XCTest
@testable import TenTea

final class TenTeaTests: XCTestCase {

    func test_TenTeaView_Looks_AsExpected() {

        let controller = UIHostingController(rootView: TenTeaView())
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }
}
