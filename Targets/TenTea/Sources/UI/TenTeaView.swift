//
//  TenTeaView
//  TenTea
//
//  Created by tom Hastik on 21/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Helpers
import Login
import Map
import SwiftUI
import XLS

/// main view for top level section of the app
///
struct TenTeaView: View {

    // MARK: - Properties

    /// main view body displaying `Form` with section selection and `Text` readme
    ///
    var body: some View {
        NavigationView {

            Form {
                Section(header: Text("tentea.view.readme".localized)
                    .font(.system(size: 11.0))
                    .padding([.bottom])) {

                        NavigationLink("tentea.view.mapview.link".localized,
                                       destination: MapView(store: Store(initialState: MapState(),
                                                                         reducer: mapReducer,
                                                                         environment: .live)))
                        NavigationLink("tentea.view.xlsview.link".localized,
                                       destination: XLSView(store: Store(initialState: XLSState(),
                                                                         reducer: xlsReducer,
                                                                         environment: .live)))
                        NavigationLink("tentea.view.loginview.link".localized,
                                       destination: LoginView(store: Store(initialState: LoginState(),
                                                                           reducer: loginReducer,
                                                                           environment: .live)))
                    }
            }
            .foregroundColor(Color.border)
            .background(Color.back)
            .navigationBarTitle("TenTea", displayMode: .inline)
            .navigationBarTitleTextColor(Color.tentea)
        }
        .accentColor(.tentea)
        .navigationViewStyle(StackNavigationViewStyle())
    }

    // MARK: - Init

    init() {
        UITableView.appearance().backgroundColor = .clear
    }
}
