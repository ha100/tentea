//
//  View+NavigationColor.swift
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import SwiftUI

extension View {

    /// Sets the text color for a navigation bar title.
    /// Supports both regular and large titles.
    ///
    /// - Parameter color: Color the title should be
    ///
    func navigationBarTitleTextColor(_ color: Color) -> some View {

        let uiColor = UIColor(color)

        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: uiColor ]
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: uiColor ]

        return self
    }
}
