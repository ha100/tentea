//
//  TenTeaApp
//  TenTea
//
//  Created by tom Hastik on 21/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import SwiftUI

@main
struct TenTeaApp: App {

    var body: some Scene {

        WindowGroup {
            TenTeaView()
        }
    }
}
