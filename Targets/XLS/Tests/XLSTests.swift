//
//  XLSTests
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation
import Model
import SnapshotTesting
import SwiftUI
import XCTest
@testable import Mocks
@testable import XLS

final class XLSTests: XCTestCase {

    var mockEntries: Array<Entry> {

        get throws {

            let mockData = try Data.fromCsvFile(name: "file_example_XLS_50")

            return String(bytes: mockData, encoding: .utf8)
                .map(Entry.parseFrom(string:)) ?? []
        }
    }

    func test_Empty_XLSView_Looks_AsExpected() {

        let store = Store(initialState: XLSState(),
                          reducer: xlsReducer,
                          environment: .mock)

        let controller = UIHostingController(rootView: XLSView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }

    func test_XLSView_Looks_AsExpected() throws {

        let entries = try XCTUnwrap(mockEntries)
        let store = Store(initialState: XLSState(rows: entries),
                          reducer: xlsReducer,
                          environment: .mock)

        let controller = UIHostingController(rootView: XLSView(store: store))
        assertSnapshot(matching: controller, as: .image(on: .iPhoneX))
    }
}
