//
//  XlsReducer
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Combine
import ComposableArchitecture
import Foundation

public let xlsReducer = Reducer<XLSState, XLSAction, XLSEnvironment> { state, action, environment in

    switch action {

        case let .textfieldChanged(string):

            state.text = string
            return .none

        case let .textfieldSubmit(string):

            return environment
                .client
                .fetchFrom(string)

        case let .receivedData(data):

            guard let string = String(bytes: data, encoding: .utf8) else {
                return .none
            }

            return environment
                .client
                .parseFrom(string)

        case let .parsedEntries(entries):

            state.rows = entries
            return .none

        case .onAppear: return .none

        case .onDisappear: return .none
    }
}
.debug()
