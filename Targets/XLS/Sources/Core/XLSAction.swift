//
//  XLSAction
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation
import Model

public enum XLSAction: AutoEquatable {

    case textfieldChanged(String)
    case textfieldSubmit(String)

    case receivedData(Data)
    case parsedEntries([Entry])

    case onAppear
    case onDisappear
}
