//
//  XLSState
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation
import Model

public struct XLSState: AutoEquatable {

    // MARK: - Properties

    // swiftlint:disable force_https
    public static let placeholder = "http://potapanie.eu/file_example_XLS_50.csv"
    // swiftlint:enable force_https

    public var rows: Array<Entry>
    public var text = ""

    // MARK: - Init

    public init(rows: Array<Entry> = []) {
        self.rows = rows
    }
}
