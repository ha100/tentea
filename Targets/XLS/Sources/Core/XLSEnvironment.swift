//
//  XLSEnvironment
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Foundation

public struct XLSEnvironment {

    public let client: XLSClient

    public init(client: XLSClient) {

        self.client = client
    }
}

public extension XLSEnvironment {

    static let live = Self(client: .live)
}
