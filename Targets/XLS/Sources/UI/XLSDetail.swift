//
//  XLSDetail
//  TenTea
//
//  Created by tom Hastik on 22/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Model
import SwiftUI

struct XLSDetail: View {

    // MARK: - Properties

    var model: Entry

    var body: some View {

        ZStack {

            Color.back
                .edgesIgnoringSafeArea(.all)

            VStack {

                HStack {

                    ZStack {
                        Circle()
                            .stroke(model.gender == .male ? .blue : .pink, lineWidth: 4)
                        Text(model.gender.rawValue.first?.description ?? "")
                    }
                    .frame(width: 40, height: 40)

                    VStack(alignment: .leading) {
                        Text(model.firstName + " " + model.lastName)
                        Text(model.country)
                            .font(.subheadline)
                    }
                }
                .padding(10)

                VStack(alignment: .leading) {
                    Text("\("xls.detail.age".localized) : \(model.age)")

                    if let date = model.date {

                        Text("\("xls.detail.date".localized) : \(DateFormatter.csvFormatter.string(from: date))")
                            .font(.subheadline)
                    }
                }
            }
        }
    }

    // MARK: - Init

    public init(entry: Entry) {

        self.model = entry
    }
}
