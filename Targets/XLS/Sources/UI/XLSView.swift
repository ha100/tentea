//
//  XLSView
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ComposableArchitecture
import Helpers
import SwiftUI

/// main view for parsing section of the app that works with `swift-parsing` framework
///
public struct XLSView: View {

    // MARK: - Properties

    private let store: Store<XLSState, XLSAction>

    /// main view body displaying `List` of objects parsed from a network resource
    ///
    public var body: some View {

        WithViewStore(store) { viewStore in

            ZStack {

                Color.back
                    .edgesIgnoringSafeArea(.all)

                VStack {

                    TextField(XLSState.placeholder,
                              text: viewStore.binding(get: { $0.text },
                                                      send: { .textfieldChanged($0) }))
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .submitLabel(.send)
                    .keyboardType(.default)
                    .padding([.leading, .trailing], 16)
                    .frame(minHeight: 30)
                    .onSubmit { viewStore.send(.textfieldSubmit(urlOrPlaceholder(text: viewStore.text))) }
                    .onAppear { viewStore.send(.onAppear) }
                    .onDisappear { viewStore.send(.onDisappear) }

                    if viewStore.state.rows.isEmpty {
                        Spacer()
                        Text("xls.view.helper".localized)
                        .foregroundColor(Color.border)
                        .multilineTextAlignment(.center)
                        .padding([.leading, .trailing], 40)
                        Spacer()

                    } else {

                        List(viewStore.rows) { entry in
                            XLSCell(entry: entry)
                        }
                    }
                }
            }
        }
    }

    // MARK: - Init

    /// `XLSView` init allowing `Store<XLSState, XLSAction>` injection for uniTesting
    ///
    /// - Parameter store: `.live` or `.mock` implementation of the store
    ///
    public init(store: Store<XLSState, XLSAction>) {

        self.store = store
    }

    // MARK: - LifeCycle

    /// lil helper to use placeholder as valid entry for empty text parameter
    ///
    /// - Parameter text: to be used to construct `URL` and obtain data from
    /// - Returns: `String` that is valid for empty text
    ///
    private func urlOrPlaceholder(text: String) -> String {
        text == "" ? XLSState.placeholder : text
    }
}
