//
//  XLSCell
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Model
import SwiftUI

/// view for cell part of the `List` displaying data parsed from network
///
struct XLSCell: View {

    // MARK: - Properties

    var model: Entry

    var body: some View {

        NavigationLink(destination: XLSDetail(entry: model)) {

            HStack {

                ZStack {
                  Circle()
                        .stroke(model.gender == .male ? .blue : .pink, lineWidth: 4)
                    Text(model.gender.rawValue.first?.description ?? "")
                }
                .frame(width: 40, height: 40)

                VStack(alignment: .leading) {
                    Text(model.firstName + " " + model.lastName)
                    Text(model.country)
                        .font(.subheadline)
                }
            }
        }
    }

    // MARK: - Init

    public init(entry: Entry) {

        self.model = entry
    }
}
