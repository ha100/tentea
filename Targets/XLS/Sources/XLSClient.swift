//
//  XLSClient
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Combine
import ComposableArchitecture
import Foundation
import Model

/// type responsible for network data retrieval
///
public struct XLSClient {

    // MARK: - Properties

    /// obtain networked resource from specified url String
    ///
    public var fetchFrom: (String) -> Effect<XLSAction, Never>
    public var parseFrom: (String) -> Effect<XLSAction, Never>

    // MARK: - Init

    public init(fetchFrom: @escaping (String) -> Effect<XLSAction, Never>,
                parseFrom: @escaping (String) -> Effect<XLSAction, Never>) {

        self.fetchFrom = fetchFrom
        self.parseFrom = parseFrom
    }
}

public extension XLSClient {

    /// default implementation reaching out to live network
    ///
    static let live = Self { urlString in

        Effect.run { subscriber in

            if let url = URL(string: urlString),
               let data = try? Data(contentsOf: url) {

                DispatchQueue.main.async {
                  subscriber.send(.receivedData(data))
                }
            }

            return AnyCancellable {
            }
        }
    } parseFrom: { string in

        Effect.run { subscriber in

            DispatchQueue.main.async {
                subscriber.send(.parsedEntries(Entry.parseFrom(string: string)))
            }

            return AnyCancellable {
            }
        }
    }
}
