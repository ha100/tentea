//
//  Strings+Localized
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

public extension String {

    var localized: String {
        NSLocalizedString(self, comment: "")
    }
}
