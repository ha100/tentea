//
//  Color+Hex
//  TenTea
//
//  Created by tom Hastik on 24/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import SwiftUI

public extension Color {

    static let tentea = Color.hex(0xc52060)
    static let back = Color.hex(0xeae2e2)
    static let border = Color.hex(0x434345)

    static func hex(_ hex: Int, alpha: Double = 1) -> Self {

        Self(red: Double((hex >> 16) & 0xff) / 255,
             green: Double((hex >> 08) & 0xff) / 255,
             blue: Double((hex >> 00) & 0xff) / 255,
             opacity: alpha)
    }
}
