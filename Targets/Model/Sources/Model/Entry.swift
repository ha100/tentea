//
//  Entry
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

/// type parsed from network resource
///
public struct Entry: AutoEquatable, Identifiable {

    // MARK: - Properties

    public var id: Int
    public var firstName: String
    public var lastName: String
    public var gender: Gender
    public var country: String
    public var age: Int
    public var date: Date?
    public var identifier: Int

    // MARK: - Init

    public init(id: String,
                firstName: String,
                lastName: String,
                gender: String,
                country: String,
                age: String,
                date: String,
                identifier: String) {

        self.id = Int(id) ?? -1
        self.firstName = firstName
        self.lastName = lastName
        self.gender = Gender(rawValue: gender) ?? .unspecified
        self.country = country
        self.age = Int(age) ?? -1
        self.date = DateFormatter.csvFormatter.date(from: date)
        self.identifier = Int(identifier) ?? -1
    }
}
