//
//  Gender
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

/// type representing `Entry` gender
///
public enum Gender: String {

    case male = "Male"
    case female = "Female"
    case unspecified
}
