//
//  DateFormatter
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation

public extension DateFormatter {

    static var csvFormatter: DateFormatter = {

        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MM/yyyy"
        dateformatter.locale = Locale.current

        return dateformatter
    }()
}
