//
//  Entry+Parsing
//  TenTea
//
//  Created by tom Hastik on 23/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import Foundation
import Parsing

public extension Entry {

    /// `Entry` parser from csv format
    ///
    /// Example
    /// =======
    ///
    /// ```
    /// 0,First Name,Last Name,Gender,Country,Age,Date,Id
    /// 1,Dulce,Abril,Female,United States,32,15/10/2017,1562
    /// ```
    ///
    static func parseFrom(string: String) -> Array<Entry> {

        string
            .components(separatedBy: "\n")
            .map { $0.components(separatedBy: ",") }
            .dropFirst()
            .map(Array.init)
            .compactMap {

                guard $0.count == 8 else { return nil }

                return Entry(id: $0[0],
                             firstName: $0[1],
                             lastName: $0[2],
                             gender: $0[3],
                             country: $0[4],
                             age: $0[5],
                             date: $0[6],
                             identifier: $0[7])
            }
    }
}
