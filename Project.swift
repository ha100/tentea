import ProjectDescription
import ProjectDescriptionHelpers

let project = Project.app(name: "TenTea",
                          platform: .iOS,
                          additionalTargets: ["XLS", "Map", "Model", "Mocks", "Login", "Helpers"])
