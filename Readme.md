[![Tuist badge](https://img.shields.io/badge/Powered%20by-Tuist-blue)](https://tuist.io)
![Swift 5.0](https://img.shields.io/badge/Swift-5.0-orange.svg?style=flat)

# about TenTea

## 100ry or story for english speaking folk

This project was a take-home assignment from an interview. Main objectives were to

  * create an app that would implement biometric security feature via `LocalAuthentication` framework
  * based on current position of the user displays a map view
  * has possibility to download excel file from a network with a single tap on a button
  * parse this excel file into table view

### patience, young grasshopper

the only part that was a real challenge and i feel like i failed was parsing of that excel file.
i was never a microsoft funboy and went the gnu linux way 22 years ago. and so last time i saw microsoft
anything was a long ago. but i took the challenge head on with no prejudices. i was quite surprised that
there is no good "out-of-the-box" solution for swift ecosystem. firstly i spent few hours researching
any available solutions, and then went for compilation of C library called `libxls`. sadly didn't went
well and after few moments i was editing compilation scripts and trying to setup cross-compilation toolchain
for `iphoneos` and `iphonesimulator`. but after some time i decided my time would be better spent elsewhere,
so i went for another tool called `quetzalxlsreader` which is basically an objective-c wrapper written in
xcode (weheheeeej) that encapsulates again `libxls`. the thought was that xcode project might be easier
to fix ... yeah. until you find yourself fixing python syntax errors in another script from xcode build phases.
this microsoft escapade took several hours out of my time and i felt like i would not finish the project into
some presentable state, so i made a decision to dump the data from the `xls` file into `csv` and go from there.
gosh i hate quiters

### devs deserve nice things

#### no xcode merge conflicts

no xcode project = no merge conflicts -> simple as that\
you gonna spin some `tuist` cli if you want to see this baby run ;) more info in `tuist` section

#### modules

the project is heavily modularised AKA "featurefull" :)\
this allows `tuist` prebuild the modules into frameworks and link them into generated xcode project. you just got to love the moment when you open xcode and it does NOT start downloading 20 linked spms and then as a cherry on top of it indexing. the actual project building is faster too since we do not build modules that did not change

### automatic code generation

the dreadful `equatability` conformance for swift enums with associated values\
`sourcery` and stencil templates are used to generate all this code by marking the types with `AutoEquatable` phantom protocol

### project uniformity

sprinkled a bit of `swiftlint` on top of all that 

### architecture

lastly architecture - everybody loves good architecture talk, so here we go\
`tca` - yeah that lil functionally unidirectional redux goodness from `pointfree` guys is in da hause.
and that is not a typo its an ali g reference. now that we have nice architecture, why not use `tuist` to
bootstrap the core for each module via `tuist scaffold TCA --name <module>` command. "its a nice"! 

## tuist setup

all that is needed - just download the `tuist`, prefetch the project package dependencies and generating the xcode project.\
then some minor changes in the project you should be already familiar with 

* select your favourite fruit device preferably real since biometry and location services are not ideal in simulator
* select `TenTea` scheme
* select valid team for `Signing & Capabilities`

### to install tuist

```bash
curl -Ls https://install.tuist.io | bash
```

### to fetch package dependencies

```bash
tuist fetch
```

## to generate xcode project file

```bash
tuist generate
```

## other usefull tuist commands

### to bootstrap a new iOS project

```bash
mkdir <MyApp>
cd <MyApp>
tuist init --platform ios --template swiftui
```

### to list available templates

```bash
tuist scaffold list
```

### to bootstrap new module

```bash
tuist scaffold module --name <module>
```

### to cache frameworks

```bash
tuist cache warm
```

## future

* we still don't parse the xfiles
* currently the localization needs fixing - one can observe problems in some test generated snapshots

* perhaps more tests with coverage reporting
* would love to auto generate enums for localization strings & assets via `swiftgen`
* some docs with `DocC` 
* ensure further coding standards with `swiftformat`
* and ci/cd could be helpfull too hmmm??
