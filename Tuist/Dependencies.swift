import ProjectDescription

let dependencies = Dependencies(
    swiftPackageManager: [
        .remote(url: "https://github.com/pointfreeco/swift-composable-architecture", requirement: .upToNextMajor(from: "0.33.1")),
        .remote(url: "https://github.com/pointfreeco/swift-parsing", requirement: .upToNextMajor(from: "0.9.2")),
        .remote(url: "https://github.com/pointfreeco/composable-core-location", requirement: .upToNextMajor(from: "0.2.0")),
        .remote(url: "https://github.com/pointfreeco/swift-snapshot-testing.git", requirement: .upToNextMajor(from: "1.9.0")),
    ],
    platforms: [.iOS]
)
