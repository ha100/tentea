import ProjectDescription

/// Project helpers are functions that simplify the way you define your project.
/// Share code to create targets, settings, dependencies,
/// Create your own conventions, e.g: a func that makes sure all shared targets are "static frameworks"
/// See https://docs.tuist.io/guides/helpers/

extension Project {

    public static let organizationName = "apperiodic OÜ"
    public static let bundleName = "eu.apperiodic"

    /// Helper function to create the Project for this ExampleApp
    ///
    public static func app(name: String,
                           platform: Platform,
                           additionalTargets: [String]) -> Project {

        var targets = makeAppTargets(name: name,
                                     platform: platform,
                                     dependencies: additionalTargets.map { TargetDependency.target(name: $0) })

        targets += additionalTargets.flatMap { makeFrameworkTargets(name: $0, platform: platform) }

        return Project(name: name,
                       organizationName: organizationName,
                       targets: targets,
                       schemes: [
                           Scheme(name: "Dev",
                                  shared: true,
                                  buildAction: .buildAction(targets: ["TenTea"]),
                                  testAction: .targets(["TenTeaTests", "XLSTests", "LoginTests"]),
                                  runAction: .runAction(configuration: "Debug", executable: "TenTea", arguments: nil)
                                  ),
                           Scheme(name: "QA",
                                  shared: true,
                                  buildAction: .buildAction(targets: ["TenTea"]),
                                  testAction: .targets(["TenTeaTests", "XLSTests", "LoginTests"]),
                                  runAction: .runAction(configuration: "Debug", executable: "TenTea", arguments: nil)
                                  ),
                           Scheme(name: "Staging",
                                  shared: true,
                                  buildAction: .buildAction(targets: ["TenTea"]),
                                  testAction: .targets(["TenTeaTests", "XLSTests", "LoginTests"]),
                                  runAction: .runAction(configuration: "Debug", executable: "TenTea", arguments: nil)
                           ),
                           Scheme(name: "Prod",
                                  shared: true,
                                  buildAction: .buildAction(targets: ["TenTea"]),
                                  testAction: .targets(["TenTeaTests", "XLSTests", "LoginTests"]),
                                  runAction: .runAction(configuration: "Release", executable: "TenTea", arguments: nil)
                           )
                        ]
                )
    }

    // MARK: - Private

    private static var debugSettings: SettingsDictionary = {

        let conditionalDebugTuistDictionary: SettingsDictionary = [
            "SWIFT_ACTIVE_COMPILATION_CONDITIONS": "DEBUG TUIST",
            "GCC_PREPROCESSOR_DEFINITIONS": "DEBUG=1 TUIST=1"
        ]

        return SettingsDictionary().merging(conditionalDebugTuistDictionary)
    }()

    private static var targetSettings: Settings = {

        Settings.settings(configurations: [
            .debug(
                name: "Debug",
                settings: debugSettings
            ),
            .release(
                name: "Release",
                settings: [:]
            )
        ])
    }()

    /// Helper function to create a framework target and an associated unit test target
    ///
    private static func makeFrameworkTargets(name: String, platform: Platform) -> [Target] {

        var testDeps: [ProjectDescription.TargetDependency] = [
            .target(name: name)
        ]

        var deps: [ProjectDescription.TargetDependency] = [
        ]

        switch name {

            case "Mocks": deps += [
                .external(name: "ComposableArchitecture"),
                .target(name: "Login"),
                .target(name: "Map"),
                .target(name: "XLS"),
                .target(name: "Model")
            ]

            case "Model": deps += [ .external(name: "Parsing") ]

            case "Map":

                deps += [
                    .external(name: "ComposableCoreLocation"),
                    .external(name: "ComposableArchitecture"),
                ]
                testDeps += [ .external(name: "SnapshotTesting") ]

            case "XLS":

                deps += [
                    .target(name: "Model"),
                    .external(name: "ComposableArchitecture"),
                ]
                testDeps += [ .external(name: "SnapshotTesting") ]

            case "Login":

                deps += [
                    .external(name: "ComposableArchitecture"),
                    .target(name: "Helpers")
                ]
                testDeps += [ .external(name: "SnapshotTesting") ]

            default: break
        }

        let sources = Target(
            name: name,
            platform: platform,
            product: .framework,
            bundleId: "\(bundleName).\(name)",
            deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
            infoPlist: .default,
            sources: ["Targets/\(name)/Sources/**"],
            resources: ["Targets/\(name)/Resources/**"],
            dependencies: deps,
            settings: targetSettings
        )

        let tests = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "\(bundleName).\(name)Tests",
            deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
            infoPlist: .default,
            sources: ["Targets/\(name)/Tests/**"],
            resources: [],
            dependencies: testDeps
        )

        return [sources, tests]
    }

    /// Helper function to create the application target and the unit test target.
    ///
    private static func makeAppTargets(name: String,
                                       platform: Platform,
                                       dependencies: [TargetDependency]) -> [Target] {

        let platform: Platform = platform

        let transportPlist: [String: InfoPlist.Value] = ["NSAllowsArbitraryLoads": true]

        let infoPlist: [String: InfoPlist.Value] = [
            "CFBundleName": "\(name)",
            "CFBundleDisplayName": "\(name)",
            "CFBundleIdentifier": "\(bundleName).\(name)",
            "CFBundleShortVersionString": "1.0",
            "CFBundleVersion": "1",
            "CFBuildVersion": "0",
            "UIMainStoryboardFile": "",
            "UILaunchStoryboardName": "LaunchScreen",
            "UILaunchScreen": [:],
            "UISupportedInterfaceOrientations": ["UIInterfaceOrientationPortrait"],
            "UIUserInterfaceStyle":"Light",
            "NSLocationWhenInUseUsageDescription": "to show your position on the map",
            "NSFaceIDUsageDescription": "we provide security options in store",
            "NSAppTransportSecurity": .dictionary(transportPlist)
            ]

        let lint = """
if which swiftlint >/dev/null; then
  swiftlint lint --no-cache --path Targets

else
  echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"
fi
"""

        let mainTarget = Target(
            name: name,
            platform: platform,
            product: .app,
            bundleId: "\(bundleName).\(name)",
            deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
            infoPlist: .extendingDefault(with: infoPlist),
            sources: ["Targets/\(name)/Sources/**"],
            resources: [
                "Targets/\(name)/Resources/**"
            ],
            scripts: [ .pre(script: lint, name: "swiftlint") ],
            dependencies: dependencies,
            settings: targetSettings
        )

        let testTarget = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "\(bundleName).\(name)Tests",
            deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
            infoPlist: .default,
            sources: ["Targets/\(name)/Tests/**"],
            dependencies: [
                .target(name: "\(name)"),
                .external(name: "SnapshotTesting")
            ]
        )

        return [mainTarget, testTarget]
    }
}
