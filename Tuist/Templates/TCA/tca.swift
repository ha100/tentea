//
//  TCA
//  Tuist
//
//  Created by tom Hastik on 21/05/2022.
//  Copyright © 2022 apperiodic OÜ. All rights reserved.
//

import ProjectDescription

let moduleName: Template.Attribute = .required("name")

func templatePath(_ path: String) -> Path {
    "Stencils/\(path)"
}

let template = Template(
    description: "TCA module template",
    attributes: [
        moduleName
    ],
    items: [

        .file(path: "Targets/\(moduleName)/Sources/Core/\(moduleName)Reducer.swift",
              templatePath: templatePath("Reducer.stencil")),

        .file(path: "Targets/\(moduleName)/Sources/Core/\(moduleName)Action.swift",
              templatePath: templatePath("Action.stencil")),

        .file(path: "Targets/\(moduleName)/Sources/Core/\(moduleName)Environment.swift",
              templatePath: templatePath("Environment.stencil")),

        .file(path: "Targets/\(moduleName)/Sources/\(moduleName)Client.swift",
              templatePath: templatePath("Client.stencil")),

        .file(path: "Targets/\(moduleName)/Sources/Core/\(moduleName)State.swift",
              templatePath: templatePath("State.stencil")),

        .file(path: "Targets/\(moduleName)/Sources/PhantomProtocols.swift",
              templatePath: templatePath("PhantomProtocols.stencil")),

        .file(path: "Targets/\(moduleName)/Tests/\(moduleName)Tests.swift",
              templatePath: templatePath("Tests.stencil"))
    ]
)
